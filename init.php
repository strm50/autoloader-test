<?php

    $classesCount = !empty($argv[1]) ? (int) $argv[1] : 50;
    
    for ($n = 1; $n <= $classesCount; ++$n) {

        $filename = "Class{$n}.php";
        $code = <<<PHP
<?php

    class Class{$n}
    {
        public \$publicVar = 'public{$n}';
        protected \$protectedVar = 'protected{$n}';
    }

PHP;

        file_put_contents($filename, $code);
    }