<?php

    $classesCount = !empty($argv[1]) ? (int) $argv[1] : 50;

    class Env
    {
        public static $reqCounter = 0;
    }

    function my_autoloader($class)
    {
        require($class . '.php');
        ++Env::$reqCounter;
    }

    spl_autoload_register('my_autoloader');
    $existCounter = 0;

    $tStart = microtime(true);
    for ($k = 0; $k < 1000; ++$k)
        for ($i = 1; $i <= $classesCount; ++$i) {
            $classname = 'Class' . $i;

            $existCounter += (int) class_exists($classname);
        }
    $tEnd = microtime(true);

    printf("reqCounter: %d\t existCounter: %d\t time: %f \n", Env::$reqCounter, $existCounter, $tEnd - $tStart);

